# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze
pkgver=5.20.2
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# mips, mips64, s390x blocked by kiconthemes
arch="all !armhf !s390x !mips !mips64"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
depends_dev="kdecoration-dev kpackage-dev ki18n-dev kguiaddons-dev kconfigwidgets-dev kwindowsystem-dev kiconthemes-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="f336b7cb9881507f8d18d380998d57ed466df419e287ec04801b1da199eed9d2ad66aece9ebdb102f383d0b465f9ef2b44cc8942006a3880d9686acd1614c520  breeze-5.20.2.tar.xz"
