# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=libplacebo
pkgver=2.72.1
pkgrel=0
pkgdesc="Reusable library for GPU-accelerated video/image rendering"
url="https://code.videolan.org/videolan/libplacebo"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	meson
	vulkan-loader-dev
	glslang-static
	glslang-dev
	shaderc-dev
	vulkan-headers
	lcms2-dev
	libepoxy-dev
	py3-mako
	"
subpackages="$pkgname-dev"
source="https://code.videolan.org/videolan/libplacebo/-/archive/v$pkgver/libplacebo-v$pkgver.tar.gz"
builddir="$srcdir/libplacebo-v$pkgver"

build() {
	abuild-meson \
		-Dvulkan=enabled \
		-Dglslang=enabled \
		-Dshaderc=enabled \
		-Dtests=true \
		-Dlcms=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="15f85631c438c641aa8913a4d5d1a4c49db5b4df7dbad425c1c672010a18f0c474a0ad3e3ad5a8ad116e70de8a3701aa45b8a10023f8ce1f8bc12dc595e56d9c  libplacebo-v2.72.1.tar.gz"
